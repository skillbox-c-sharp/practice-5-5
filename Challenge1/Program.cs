﻿namespace Challenge1;
class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Введите предложение:");
        string userString = Console.ReadLine()!;
        string[] splitWords = SplitUserString(userString);
        PrintSplitWords(splitWords);
    }

    static string[] SplitUserString(string str)
    {
        return str.Split(" ");
    }

    static void PrintSplitWords(string[] words)
    {
        Console.WriteLine();
        foreach (var e in words)
        {
            Console.WriteLine(e);
        }
        Console.ReadKey();
    }
}

