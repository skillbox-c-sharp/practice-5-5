﻿namespace Challenge2;
class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Введите предложение:");
        string userString = Console.ReadLine()!;
        string[] reverseStringWords = ReverseString(userString);
        PrintSplitWords(reverseStringWords);
    }

    static string[] ReverseString(string str)
    {
        //string[] splitWords = SplitUserString(str);
        //Array.Reverse(splitWords);
        //return splitWords;

        string[] splitWords = SplitUserString(str);
        string[] reverseSplitWords = new string[splitWords.Length];
        for (int i = 0; i < splitWords.Length; i++)
        {
            reverseSplitWords[splitWords.Length - i - 1] = splitWords[i];
        }
        return reverseSplitWords;
    }

    static string[] SplitUserString(string str)
    {
        return str.Split(" ");
    }

    static void PrintSplitWords(string[] words)
    {
        Console.WriteLine();
        foreach (var e in words)
        {
            Console.WriteLine(e);
        }
        Console.ReadKey();
    }
}

